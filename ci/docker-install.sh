#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
#apt-get update -yqq
#apt-get install git -yqq

# install php, to run phpunit..
#sudo apt-get install php7.0
#apt-get update
#apt-get install php

# Install phpunit, the tool that we will use for testing
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit-6.phar
chmod +x /usr/local/bin/phpunit

#wget -O phpunit https://phar.phpunit.de/phpunit-6.phar
#chmod +x phpunit
#/phpunit --version

# Install mysql driver
# Here you can install any other extension that you need
#docker-php-ext-install pdo_mysql